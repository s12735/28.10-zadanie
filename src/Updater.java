
public class Updater {
	private Cache cache;
	private Object lock = new Object();

	public Updater(Cache cache) {
		this.cache = cache;
	}

	public void Update() {
		synchronized (lock) {
			cache.refreshCache(/* values from database */);
		}
	}
}
