
public class EnumValue {
	private String id;
	private String enumID;
	private String code;
	private String value;
	private EnumerationName EnumerationName;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getEnumID() {
		return enumID;
	}
	public void setEnumID(String enumID) {
		this.enumID = enumID;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public EnumerationName getEnumerationName() {
		return EnumerationName;
	}
	public void setEnumerationName(EnumerationName enumerationName) {
		EnumerationName = enumerationName;
	}
	@Override
	public String toString() {
		return "EnumValue [id=" + id + ", enumID=" + enumID + ", code=" + code + ", value=" + value
				+ ", EnumerationName=" + EnumerationName + "]";
	}
	


}
