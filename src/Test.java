import java.util.Timer;

public class Test {
	public static void main(String[] args) {
		Cache cache = Cache.getInstance();
		Updater updater = new Updater(cache);
		
		
		Timer timer = new Timer(true);
		timer.schedule(new UpdateTask(updater), 5*60*1000);
		
	}
}
