import java.util.TimerTask;

public class UpdateTask extends TimerTask {
	private Updater update;
	
	public UpdateTask(Updater updater) {
		this.update = updater;
	}
	@Override
	public void run() {
		update.Update();
	}
}
