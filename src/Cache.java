import java.util.HashMap;
import java.util.Map;

import javax.swing.plaf.BorderUIResource.EmptyBorderUIResource;

public class Cache {
	private Map<String, EnumValue> items = new HashMap<>();
	private static Cache instance;

	private Cache() 
	{}

	public static Cache getInstance() {
		if (instance == null) {
			instance = new Cache();
		}
		return instance;
	}

	public void clear() {
		items.clear();
	}
	
	public void add(String key, EnumValue value){
		items.put(key, value);
	}
	
	public void get(String key){
		System.out.println(items.get(key));
	}
	
	public void refreshCache(Map<String, EnumValue> values){
		clear();
		this.items = values;
	}
}
